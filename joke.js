(function($){
    //almost main thing of this joke. Random in 0 to digitsNumber interval.
    var getRandom = function(digitsNumber){
        return Math.floor( Math.random() * digitsNumber);
    }

    var hoverBehaviors = [],
        clickBehaviors = [],
        timerHelper = {
            timers: [],
            MAX_TIMERS_NUM: 3
        };

    $(function(){
        var collectedData = {
                $a: $('a')
            },
            applyBehavior = function(context, behaviors){
                if(behaviors.length > 0) {
                    var randomBehaviorIndex = getRandom(behaviors.length);
                    behaviors[randomBehaviorIndex].call(context, collectedData);
                }
            };

        // triggers random hover behavior on link mouseleave
        collectedData.$a.mouseleave(function(){
            applyBehavior(this, hoverBehaviors);
        });

        //triggers random click behavior on link click
        collectedData.$a.on('click', function(e){
            var $this = $(this);
            if($this.attr('href') !== '#') {
                applyBehavior(this, clickBehaviors);
                return false;
            }
            return true;
        });
    });



    //TODO: HAVE A LOT OF FUN THERE !!!!!! just push your behavior in list.

    //shuffles all images on page
    hoverBehaviors.push(function(){
        var shuffle = function(o) {
            for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
            return o;
        };

        var images = $('img');
        var links = [];
        $.each(images, function(){
            links.push($(this).attr('src'));
        });
        var shuffledLinks = shuffle(links);
        var i = 0;
        $.each(images, function(){
            $(this).attr('src', shuffledLinks[i]);
            i++;
        });
    });

    //toggle fade MAX_TIMERS_NUM of links every second
    hoverBehaviors.push(function(){
        var $this = $(this);
        if(timerHelper.timers.length >= timerHelper.MAX_TIMERS_NUM) {
            var index = getRandom(timerHelper.timers.length);
            clearInterval(timerHelper.timers[index]);
            timerHelper.timers.splice(index, 1);
        }
        timerHelper.timers.push(setInterval(function() {
            $this.fadeToggle('slow');
        }, 1000));
    });

    //makes all links random
    clickBehaviors.push(function(data){
        var $a = data.$a;
        var customHref = $a[getRandom($a.length)].href;
        window.location.href = customHref;

    });
}(jQuery));